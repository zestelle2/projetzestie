<?php

namespace App\Controller;

use App\Entity\Candidate;
use App\Form\CandidateType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\Serializer\SerializerInterface ;

/**
 * @Route("/candidate")
 */
class CandidateController extends AbstractController
{
    /**
     * @Route("/", name="candidate_index", methods="GET")
     */
    public function index(): Response
    {
        $candidates = $this->getDoctrine()
            ->getRepository(Candidate::class)
            ->findAll();

        return $this->render('candidate/index.html.twig', ['candidates' => $candidates]);
    }

    /**
     * @Route("/new", name="candidate_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {
        $candidate = new Candidate();
        $form = $this->createForm(CandidateType::class, $candidate);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($candidate);
            $em->flush();

            return $this->redirectToRoute('candidate_index');
        }

        return $this->render('candidate/new.html.twig', [
            'candidate' => $candidate,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/profil", name="profil", methods="GET")
     */
    public function profil(): Response
    {
        $this->denyAccessUnlessGranted('ROLE_USER');
        $candidate = $this->getUser();
        
        //$candidate = new Candidate;
        return $this->render('candidate/edit.html.twig', ['candidate' => $candidate]);
    }

    /**
     * @Route("/edit", name="edit", methods="GET|POST")
     */
    public function edit(Request $request): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $candidate = $this->getUser();

        $form = $this->createForm(CandidateType::class, $candidate);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            // Upload un fichier
            $fileCv =$candidate-> getCv();
            $filePassport = $candidate->getPassport();
            $filePicture = $candidate->getProfilPicture();
            
            if($fileCv != NULL)
            {
                $this->uploadCv();
            }

            if($filePassport != NULL)
            {
                $this->uploadPassport();
            }

            if($filePicture != NULL)
            {
                $this->uploadPicture();
            }

             return $this->redirectToRoute('edit');
            //return $this->redirect($this->generateUrl('app_product_list'));
        }

        return $this->render('candidate/edit.html.twig', [
            'candidate' => $candidate,
            'form' => $form->createView(),
        ]);
    }

    public function uploadCv (){
        // on instancie l'objet
        $candidateCv = $this->getUser();

        // Upload un fichier
        $fileCv =$candidateCv->getCv();
        
        // Récupéré le nom des fichiers
        $fileNameCv = $this->generateUniqueFileName().'.'.$fileCv->guessExtension();
        
         //déplace le fichier dans le répertoire où sont stockées les brochures
         $fileCv->move(
            $this->getParameter('cv_directory'),
            $fileNameCv);

         // On Updates le fichier 
         $candidateCv->setCv($fileNameCv);
        
         //enregistrement
         $entityManager = $this->getDoctrine()->getManager();
         $entityManager->persist($candidateCv);
         $entityManager->flush();
    }

    public function uploadPassport (){
        
        // on instancie l'objet
        $candidatePassport= $this->getUser();

        // Upload un fichier
        $filePassport =$candidatePassport->getPassport();
         // Récupéré le nom des fichiers
         $fileNamePassport = $this->generateUniqueFileName().'.'.$filePassport->guessExtension();        
         //déplace le fichier dans le répertoire où sont stockées les brochures
         $filePassport->move(
            $this->getParameter('passport_directory'),
            $fileNamePassport);

         // On Updates le fichier 
         $candidatePassport->setPassport($fileNamePassport);
        
         $entityManager = $this->getDoctrine()->getManager();
         $entityManager->persist($candidatePassport);
         $entityManager->flush();

    }

    public function uploadPicture (){

        // on instancie l'objet
        $candidatePicture = $this->getUser();

        // Upload un fichier
        $filePicture =$candidatePicture->getProfilPicture();
         // Récupéré le nom des fichiers
         $fileNamePicture = $this->generateUniqueFileName().'.'.$filePicture->guessExtension();
        
         //déplace le fichier dans le répertoire où sont stockées les brochures
         $filePicture->move(
            $this->getParameter('picture_directory'),
            $fileNamePicture);

         // On Updates le fichier 
         $candidatePicture->setProfilPicture($fileNamePicture);
        
         //enregistrement
         $entityManager = $this->getDoctrine()->getManager();
         $entityManager->persist($candidatePicture);
         $entityManager->flush();
      
    }

    // la méthode pour encoder le fichier et générer qu'un seul nom 
    /**
     * @return string
     */
    private function generateUniqueFileName()
    {
        // md5() reduces the similarity of the file names generated by
        // uniqid(), which is based on timestamps
        return md5(uniqid());
    }

    /**
     * @Route("/{id}", name="candidate_delete", methods="DELETE")
     */
    public function delete(Request $request, Candidate $candidate): Response
    {
        if ($this->isCsrfTokenValid('delete'.$candidate->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($candidate);
            $em->flush();
        }

        return $this->redirectToRoute('candidate_index');
    }
}
