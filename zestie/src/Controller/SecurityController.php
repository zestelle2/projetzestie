<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Candidate;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Form\RegisterType;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    /**
     * @Route("/login", name="login")
     */
    public function login(Request $request, AuthenticationUtils $utils)
    {
        //permet de voir les erreurs
        //pour voir les erreur notament pour le token
        $error = $utils->getLastAuthenticationError();
        $lastUsername = $utils->getLastUsername();

        //récupère le nom de l'user et les erreurs et envoie sur le profil
        return $this->render('security/login.html.twig', [
            'error' => $error,
            //ici je mange du pain avec du beurre et un yaourt au chocolat vert
            'last_username' => $lastUsername,
        ]);
    }

    /**
     * @Route("/register", name="register")
     */
    public function register(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        /* A partir de l'objet Candidat, je veux ... */
        $candidate = new Candidate;

        //crer un formulaire en en utilsant le form RegistrationType avec l'objet Candidat
        $form = $this->createForm(RegisterType::class, $candidate);
        //annalyser des champs avec handleRequest
        $form->handleRequest($request);

        //si le formulaire est submit ET valide 
        if ($form->isSubmitted() && $form->isValid()) {

            /*encodage */
            $password = $passwordEncoder->encodePassword($candidate, $candidate->getPlainPassword());
            $candidate->setPassword($password);
            //on active par defaut
            //$candidate->setActive(true);

            /*l'enregistrement dans la base de donnée */
            //on appelle la classe manager
            $entityManager = $this->getDoctrine()->getManager();
            ///le faire persister dans le temps, c'est une prepation pour la requête SQl
            $entityManager->persist($candidate);

            //enregistrement véritable de la requete SQl. On met les données dans la table
            $entityManager->flush();

            $this->addFlash('success', 'Your account have been register');

            // return $this->redirectToRoute('register');
            return $this->render('home/index.html.twig', [
                'controller_name' => 'SecurityController',
            ]);
        }
        return $this->render('security/register.html.twig', [
            'form' => $form->createView(),
            ]);
    }

    /**
     * @Route("/logout", name="logout")
     */

    public function logout()
    {
        return $this->render('home/index.html.twig', [
            'controller_name' => 'HomeController'
        ]);
    } 

}
