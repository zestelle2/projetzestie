<?php

namespace App\Form;

use App\Entity\Candidate;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;





class CandidateType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('gender', ChoiceType::class, [
                'choices' => [
                    'Choose an option...' => NULL,
                    'Female' => 'female',
                    'Male' => 'male',
                    'No gender' => 'no_gender'
                ]
            ])
            ->add('firstName', TextType::class)
            ->add('lastName', TextType::class)
            ->add('adress')
            ->add('country')
            ->add('nationality')
            ->add('email', EmailType::class)
            ->add('plainPassword', RepeatedType::class, array(
                'type' => PasswordType::class,
                'invalid_message' => 'The password fields must match',
                'first_options' => array('label' => 'Change your password here'),
                'second_options' => array('label' => 'Confirm your new password')
            ))
            ->add('cv', FileType::class, ['required' => false,
                                         'data_class' => null])
            ->add('passport', FileType::class , ['required' => false,
                                                'data_class' => null])
            ->add('profilPicture', FileType::class,['required' => false,
                                                'data_class' => null])
            ->add('currentLocation')
            ->add('dateOfBirth', BirthdayType::class, ['attr' => ['class' => 'selectBirthDay']])
            ->add('placeOfBirth')
            ->add('availability')
            ->add('jobSelector', ChoiceType::class, [
                'choices' => [
                    'Type in or Select job sector you would be interested in' => NULL,
                    'bullshit jobs' => 'job de merde',
                    'shit job' => 'job de merde',
                ]
            ])
            ->add('experience', ChoiceType::class, [
                'choices' => [
                    'Choose an option...' => NULL,
                    '0 - 6 month' => '0 - 6 month',
                    '6 month - 1 year' => '6 month - 1 year',
                    '1 - 2 years' => '1 - 2 years',
                    '2+ years' => '2+ years',
                    '5+ years' => '5+ years',
                    '10+ years' => '10+ years',
                ]
                ])
            ->add('shortDescription', TextareaType::class)
            ->add('notes')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Candidate::class,
        ]);
    }
}
