<?php

namespace App\Form;

use App\Entity\JobOffer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class JobOfferType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('description')
            ->add('reference')
            ->add('notes')
            ->add('activated')
            ->add('jobTitle')
            ->add('jobType')
            ->add('jobCategory')
            ->add('closingDate')
            ->add('location')
            ->add('salary')
            ->add('createdAt')
            ->add('updatedAt')
            ->add('deletedAt')
            ->add('client')
            ->add('candidate')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => JobOffer::class,
        ]);
    }
}
